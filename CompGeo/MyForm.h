#pragma once
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <Windows.h>
#include <math.h>
#include <msclr\marshal.h>  
#include <msclr\marshal_cppstd.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>  
#include <string.h> 
using namespace cv;

//Find angle relative to x-axis. CCW direction
double angleOf(cv::Point a, cv::Point b);

//Find if point is left of 2 other points; 1 = left; -1 = right; 0 = collinear
int leftOf(cv::Point a, cv::Point b, cv::Point c);

bool point_operatorX(const Point &a, const Point &b);
bool point_operatorY(const Point &a, const Point &b);

double distance(cv::Point lineStart, cv::Point lineEnd, cv::Point point);
void FindHull(std::vector<cv::Point> points, cv::Point A, cv::Point B, std::vector<cv::Point> &convexHull, Mat &imageOut);

static int timeComplexity = 0;
static bool fullAni;
static bool partialAni;

namespace CompGeo {

    using namespace System;
    using namespace System::ComponentModel;
    using namespace System::Collections;
    using namespace System::Windows::Forms;
    using namespace System::Data;
    using namespace System::Drawing;

    /// <summary>
    /// Summary for MyForm
    /// </summary>
    Mat imageIn; //save the original image so we can reset it as much as we want
    Mat imageOut; //use this when manipulating the image
    Mat contourOut; //contours image
    const int MAX_THRESHOLD_LEVEL = 255;
    std::vector<std::vector<cv::Point>> contours; //points go here
    std::vector<cv::Point> completeContour; //merge all contour points here
    public ref class MyForm : public System::Windows::Forms::Form
    {
    public:
        MyForm(void)
        {
            InitializeComponent();
            //
            //TODO: Add the constructor code here
            //
            noAnimation->Checked = true;
            MessageBox::Show("Note:\nNo animation = only draw convex hull points\n\nPartial animation = only shows cadidate convex hull points that advance the algorithm\n\nFull animation = shows all candidate convex hull points as they are tested.\nFull animation will take a long time. You have been warned.");
        }

    protected:
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        ~MyForm()
        {
            if (components)
            {
                delete components;
            }
        }
    private: System::Windows::Forms::Button^  button1;
    protected:
    private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
    private: System::Windows::Forms::Button^  button2;
    private: System::Windows::Forms::GroupBox^  groupBox1;
    private: System::Windows::Forms::TrackBar^  trackBar1;
    private: System::Windows::Forms::Button^  button3;
    private: System::Windows::Forms::Button^  button4;
    private: System::Windows::Forms::Button^  button5;
    private: System::Windows::Forms::Button^  button6;
    private: System::Windows::Forms::RadioButton^  fullAnimation;
    private: System::Windows::Forms::RadioButton^  partialAnimation;
    private: System::Windows::Forms::RadioButton^  noAnimation;





    protected:

    private:
        /// <summary>
        /// Required designer variable.
        /// </summary>
        System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->button1 = (gcnew System::Windows::Forms::Button());
            this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
            this->button2 = (gcnew System::Windows::Forms::Button());
            this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            this->trackBar1 = (gcnew System::Windows::Forms::TrackBar());
            this->button3 = (gcnew System::Windows::Forms::Button());
            this->button4 = (gcnew System::Windows::Forms::Button());
            this->button5 = (gcnew System::Windows::Forms::Button());
            this->button6 = (gcnew System::Windows::Forms::Button());
            this->fullAnimation = (gcnew System::Windows::Forms::RadioButton());
            this->partialAnimation = (gcnew System::Windows::Forms::RadioButton());
            this->noAnimation = (gcnew System::Windows::Forms::RadioButton());
            this->groupBox1->SuspendLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->BeginInit();
            this->SuspendLayout();
            // 
            // button1
            // 
            this->button1->Location = System::Drawing::Point(13, 12);
            this->button1->Name = L"button1";
            this->button1->Size = System::Drawing::Size(65, 54);
            this->button1->TabIndex = 0;
            this->button1->Text = L"Open Image";
            this->button1->UseVisualStyleBackColor = true;
            this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
            // 
            // openFileDialog1
            // 
            this->openFileDialog1->FileName = L"openFileDialog1";
            // 
            // button2
            // 
            this->button2->Location = System::Drawing::Point(84, 12);
            this->button2->Name = L"button2";
            this->button2->Size = System::Drawing::Size(65, 54);
            this->button2->TabIndex = 1;
            this->button2->Text = L"Apply Threshold";
            this->button2->UseVisualStyleBackColor = true;
            this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
            // 
            // groupBox1
            // 
            this->groupBox1->Controls->Add(this->trackBar1);
            this->groupBox1->Location = System::Drawing::Point(12, 72);
            this->groupBox1->Name = L"groupBox1";
            this->groupBox1->Size = System::Drawing::Size(623, 66);
            this->groupBox1->TabIndex = 2;
            this->groupBox1->TabStop = false;
            this->groupBox1->Text = L"Threshold Level";
            // 
            // trackBar1
            // 
            this->trackBar1->Location = System::Drawing::Point(7, 19);
            this->trackBar1->Maximum = 255;
            this->trackBar1->Name = L"trackBar1";
            this->trackBar1->Size = System::Drawing::Size(603, 45);
            this->trackBar1->TabIndex = 0;
            this->trackBar1->Scroll += gcnew System::EventHandler(this, &MyForm::trackBar1_Scroll);
            // 
            // button3
            // 
            this->button3->Location = System::Drawing::Point(420, 12);
            this->button3->Name = L"button3";
            this->button3->Size = System::Drawing::Size(65, 54);
            this->button3->TabIndex = 3;
            this->button3->Text = L"Reset Image";
            this->button3->UseVisualStyleBackColor = true;
            this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
            // 
            // button4
            // 
            this->button4->Location = System::Drawing::Point(155, 12);
            this->button4->Name = L"button4";
            this->button4->Size = System::Drawing::Size(65, 54);
            this->button4->TabIndex = 4;
            this->button4->Text = L"Extract Contours";
            this->button4->UseVisualStyleBackColor = true;
            this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
            // 
            // button5
            // 
            this->button5->Location = System::Drawing::Point(226, 12);
            this->button5->Name = L"button5";
            this->button5->Size = System::Drawing::Size(91, 54);
            this->button5->TabIndex = 5;
            this->button5->Text = L"Convex Hull Jarvis/Giftwrap";
            this->button5->UseVisualStyleBackColor = true;
            this->button5->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
            // 
            // button6
            // 
            this->button6->Location = System::Drawing::Point(323, 12);
            this->button6->Name = L"button6";
            this->button6->Size = System::Drawing::Size(91, 54);
            this->button6->TabIndex = 6;
            this->button6->Text = L"Convex Hull QuickHull";
            this->button6->UseVisualStyleBackColor = true;
            this->button6->Click += gcnew System::EventHandler(this, &MyForm::button6_Click);
            // 
            // fullAnimation
            // 
            this->fullAnimation->AutoSize = true;
            this->fullAnimation->Location = System::Drawing::Point(532, 49);
            this->fullAnimation->Name = L"fullAnimation";
            this->fullAnimation->Size = System::Drawing::Size(90, 17);
            this->fullAnimation->TabIndex = 7;
            this->fullAnimation->TabStop = true;
            this->fullAnimation->Text = L"Full Animation";
            this->fullAnimation->UseVisualStyleBackColor = true;
            this->fullAnimation->CheckedChanged += gcnew System::EventHandler(this, &MyForm::fullAnimation_CheckedChanged);
            // 
            // partialAnimation
            // 
            this->partialAnimation->AutoSize = true;
            this->partialAnimation->Location = System::Drawing::Point(532, 31);
            this->partialAnimation->Name = L"partialAnimation";
            this->partialAnimation->Size = System::Drawing::Size(103, 17);
            this->partialAnimation->TabIndex = 8;
            this->partialAnimation->TabStop = true;
            this->partialAnimation->Text = L"Partial Animation";
            this->partialAnimation->UseVisualStyleBackColor = true;
            this->partialAnimation->CheckedChanged += gcnew System::EventHandler(this, &MyForm::partialAnimation_CheckedChanged);
            // 
            // noAnimation
            // 
            this->noAnimation->AutoSize = true;
            this->noAnimation->Location = System::Drawing::Point(532, 12);
            this->noAnimation->Name = L"noAnimation";
            this->noAnimation->Size = System::Drawing::Size(88, 17);
            this->noAnimation->TabIndex = 9;
            this->noAnimation->TabStop = true;
            this->noAnimation->Text = L"No Animation";
            this->noAnimation->UseVisualStyleBackColor = true;
            // 
            // MyForm
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(647, 153);
            this->Controls->Add(this->noAnimation);
            this->Controls->Add(this->partialAnimation);
            this->Controls->Add(this->fullAnimation);
            this->Controls->Add(this->button6);
            this->Controls->Add(this->button5);
            this->Controls->Add(this->button4);
            this->Controls->Add(this->button3);
            this->Controls->Add(this->groupBox1);
            this->Controls->Add(this->button2);
            this->Controls->Add(this->button1);
            this->Name = L"MyForm";
            this->Text = L"Convex Hull";
            this->groupBox1->ResumeLayout(false);
            this->groupBox1->PerformLayout();
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion

        //open image
    private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
        if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
        {
            System::IO::StreamReader ^ sr = gcnew
                System::IO::StreamReader(openFileDialog1->FileName);
            MessageBox::Show(openFileDialog1->FileName);
            sr->Close();

            std::string standardString = msclr::interop::marshal_as<std::string>(openFileDialog1->FileName);
            imageIn = imread(standardString);
            cvtColor(imageIn, imageOut, CV_BGR2GRAY);
            imshow("Image", imageOut);

        }
    }
             //reset image
    private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
        cvtColor(imageIn, imageOut, CV_BGR2GRAY);
        imshow("Image", imageOut);
        contours.clear();
        completeContour.clear();

    }
             //threshold button
    private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
        threshold(imageOut, imageOut, (double)trackBar1->Value, (double)MAX_THRESHOLD_LEVEL, THRESH_BINARY);
        imshow("Image", imageOut);
    }
    
             //trackbar change
    private: System::Void trackBar1_Scroll(System::Object^  sender, System::EventArgs^  e) {
        groupBox1->Text = "Threshold Level " + trackBar1->Value.ToString();
    }
    
            //Contour button
    private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
        findContours(imageOut, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE); //find contours
        Scalar color(255, 255, 255); //set color to white for contours
        contourOut = Mat::zeros(imageIn.rows, imageIn.cols, CV_8UC3); //resize contourOut which is currently empty
        drawContours(contourOut, contours, -1, color); //draw the contours on to contourOut
        cvtColor(contourOut, imageOut, CV_BGR2GRAY); //convert to grayscale
        imshow("Image", imageOut); //show the grayscale image

                                   //grab original vector of vectors of contours
        for (int i = 0; i < contours.size(); i++)
        {
            for (int j = 0; j < contours[i].size(); j++)
            {
                completeContour.push_back(contours[i][j]); //merge all contours into one list
            }
        }

    }
             //convex hull button
    private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
        timeComplexity = 0;

        std::sort(completeContour.begin(), completeContour.end(), &point_operatorX); //sort the list by x coordinates
        std::vector<cv::Point> CH; //vector for the points of the convex hull
        cv::Mat test; //image for the  convex hull
        cv::Mat tempMat; //temp mat for visualization
        imageOut.copyTo(test);
        test.copyTo(tempMat);

        /*UNCOMMENT BLOCK BELOW IF YOU WANT TO PRINT CONVEX HULL COORDS TO TEXT FILE*/
        //std::ofstream coords; //output coordinates into textfile (did this to confirm that my operator overload did indeed work with the sorting algo)
        //coords.open("CONTOUR COORDINATES.txt", std::ios::out);
        //for (int i = 0; i < completeContour.size(); i++)
        //{
        //    coords << completeContour[i].x << " " << completeContour[i].y << "\n";
        //}
        //coords.close();



        //say that there aren't enough points to make a CH and exit
        if (completeContour.size() < 3)
        {
            MessageBox::Show("Not enough points to construct\na convex hull.");
            return;
        }


        /*
        Jarvis' March/Giftwrapping Algorithm - Done with leftOf tests/CCW calculation as per
        Exercise 2.18 on page 43 of Discrete and Computational Geometry book.

        https://en.wikipedia.org/wiki/Gift_wrapping_algorithm
        */

        /*Find lowest and leftmost point*/
        int lowestX = 0;
        int count = 0;
        int lowestLeftMost = 0;

        while (completeContour[count].x == completeContour[0].x)
        {
            if (completeContour[count].y < completeContour[lowestX].y)
                lowestX = count;
            count++;
        }


        int b = lowestX; //first
        int c = 0; //second point
        cv::Scalar white; //scalar needed for drawing line
        white = (255, 255, 255);

        do
        {
            c = ((b + 1) % completeContour.size()); //move to next x coordinate
            for (int i = 0; i < completeContour.size(); i++) //for all x coordinates
            {
                //for full animation
                if (fullAnimation->Checked)
                {
                    cv::line(test, completeContour[b], completeContour[i], white); //draw current line of smallest angle
                    imshow("Convex Hull - Jarvis/Giftwrap", test);
                    waitKey(1);
                    tempMat.copyTo(test); //rewrite back to blank
                }

                timeComplexity++; //increment time complexity

                if (leftOf(completeContour[b], completeContour[c], completeContour[i]) == 1) //check if CCW
                {
                    c = i; //c is left of a and b; update and find something even more leftOf a and new b
                           /*Draw CCW angles*/
                    //for partial animation
                    if (partialAnimation->Checked)
                    {
                        cv::line(test, completeContour[b], completeContour[i], white); //draw current line of smallest angle
                        imshow("Convex Hull - Jarvis/Giftwrap", test);
                        waitKey(1);
                        tempMat.copyTo(test); //rewrite back to blank
                    }
                }
            }
            //draw only the CH
            cv::line(test, completeContour[b], completeContour[c], white); //permanently update the CH line to the image
            waitKey(1);
            imshow("Convex Hull - Jarvis/Giftwrap", test);
            test.copyTo(tempMat); //copy permanent changes to temp image
            b = c; //update to most CCW/leftOf point
        } while (b != lowestX);
        MessageBox::Show("Number of operations: " + timeComplexity.ToString());
        return;

        /*UNCOMMENT BLOCK BELOW IF YOU WANT TO PRINT CONVEX HULL COORDS TO TEXT FILE*/
        //std::ofstream CHt; //output coordinates into textfile (did this to confirm that my operator overload did indeed work with the sorting algo)
        //CHt.open("CH COORDINATES.txt", std::ios::out);
        //for (int i = 0; i < CH.size(); i++)
        //{
        //    coords << CH[i].x << " " << CH[i].y << "\n";
        //}
        //CHt.close();


    }

            //QuickHull button
    private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
        //Algorithm modeled after pseudo code at
        //https://en.wikipedia.org/wiki/Quickhull
        timeComplexity = 0;
        //sort the contour
        std::sort(completeContour.begin(), completeContour.end(), &point_operatorX);

        //find the min max of X
        int lowestX = 0;
        int highestX = (completeContour.size() - 1);

        //push on the first two points
        std::vector<cv::Point> CH;
        CH.push_back(completeContour[lowestX]);
        CH.push_back(completeContour[highestX]);

        //create the sets of points for quickhull to recursively add on
        std::vector<cv::Point> S1;
        std::vector<cv::Point> S2;

        for (int i = 1; i < completeContour.size() - 1; i++) //for each point that are not the min and max of X
        {
            if (leftOf(completeContour[lowestX], completeContour[highestX], completeContour[i]) == -1)
                S1.push_back(completeContour[i]);//push on to set 1 if to the right of A->B
            if (leftOf(completeContour[highestX], completeContour[lowestX], completeContour[i]) == -1)
                S2.push_back(completeContour[i]);//push on to set 2 if to the right of B->A
        }

        cv::line(imageOut, completeContour[lowestX], completeContour[highestX], (255, 255, 255));
        imshow("Convex Hull - QuickHull", imageOut);


        //recursively find the hull of each half
        FindHull(S1, completeContour[lowestX], completeContour[highestX], CH, imageOut);
        FindHull(S2, completeContour[highestX], completeContour[lowestX], CH, imageOut);
        MessageBox::Show("Number of operations: " + timeComplexity.ToString());
        return;
    }

    private: System::Void fullAnimation_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
        fullAni = fullAnimation->Checked;
    }

    private: System::Void partialAnimation_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
        partialAni = partialAnimation->Checked;
}
};
}

//recursive call for quickhull
void FindHull(std::vector<cv::Point> points, cv::Point A, cv::Point B, std::vector<cv::Point> &convexHull, Mat &imageOut)
{
    if (points.size() == 0)
    {
        return; //return because there are no more recursive calls to do
    }
    //temp image
    Mat tempMat;
    imageOut.copyTo(tempMat);
    
    //find fathest point from
    int farthestDistance = 0;
    int farthestPoint = 0;
    for (int i = 0; i < points.size(); i++) //for all points in the set
    {
        //full animation
        if (fullAni)
        {
            cv::line(imageOut, A, points[i], (255, 255, 255));
            cv::line(imageOut, B, points[i], (255, 255, 255));
            imshow("Convex Hull - QuickHull", imageOut);
            waitKey(1);
            tempMat.copyTo(imageOut);
        }

        timeComplexity++; //increment number of operations
        
        double currentDistance = distance(A, B, points[i]);
        if (currentDistance > farthestDistance) //find the farthest point
        {
            farthestDistance = currentDistance;
            farthestPoint = i;

            //partial animation
            if (partialAni)
            {
                cv::line(imageOut, A, points[i], (255, 255, 255));
                cv::line(imageOut, B, points[i], (255, 255, 255));
                imshow("Convex Hull - QuickHull", imageOut);
                waitKey(1);
                tempMat.copyTo(imageOut);
            }
        }
    }

    //add farthest point to convex hull
    convexHull.push_back(points[farthestPoint]);
    cv::Point C = points[farthestPoint];


    //draw lines
    cv::line(imageOut, A, C, (255, 255, 255));
    cv::line(imageOut, B, C, (255, 255, 255));
    imshow("Convex Hull - QuickHull", imageOut);

    //sets for next recursive call
    std::vector<cv::Point> S1;
    std::vector<cv::Point> S2;

    for (int i = 0; i < points.size(); i++) //for all sets
    {
        if (leftOf(A, C, points[i]) == -1)
            S1.push_back(points[i]); //push on set 1 if to the right of A->C
        else if (leftOf(C, B, points[i]) == -1)
            S2.push_back(points[i]); //push on set 2 if to the right of C->B
    }

    //perform recursive calls
    FindHull(S1, A, C, convexHull, imageOut);
    FindHull(S2, C, B, convexHull, imageOut);
    return;

}

//distance from line
double distance(cv::Point lineStart, cv::Point lineEnd, cv::Point point)
{
    //Equaton found at
    //https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    double hypotenuse = _hypot(lineEnd.x - lineStart.x, lineEnd.y - lineStart.y); //distance from A to B
    double distance = (double)((point.x - lineStart.x) * (lineEnd.y - lineStart.y) - (point.y - lineStart.y) * (lineEnd.x - lineStart.x)) / hypotenuse;
    return abs(distance);
}

//calculate angle
double angleOf(cv::Point a, cv::Point b)
{
    return (double)(-(double)(b.x - a.x) / (double)(b.y - a.y));
}

//determine if a point is to the left of a line/CCW
int leftOf(cv::Point a, cv::Point b, cv::Point c)
{
    //Calculating orientation of 3 points
    //https://www.cs.cmu.edu/~quake/robust.html

    int det = ((a.x - c.x) * (b.y - c.y)) - ((b.x - c.x) * (a.y - c.y));
    if (det > 0)
        return 1; //leftOf/CCW
    if (det < 0)
        return -1; //rightOf/CW
    return 0; //collinear
}

//so the sorting algorithm can sort
bool point_operatorX(const Point &a, const Point &b)
{
    return (a.x < b.x);
}

//so the sorting algorithm can sort
bool point_operatorY(const Point &a, const Point &b)
{
    return (a.y > b.y);

}
