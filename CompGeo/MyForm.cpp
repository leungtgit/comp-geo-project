
#include "MyForm.h"
#include <fstream>
#include <iostream>

using namespace System;
using namespace System::Windows::Forms;

[STAThread]
void Main(array<System::String^>^ args)
{
    Application::EnableVisualStyles();
    Application::SetCompatibleTextRenderingDefault(false);

    CompGeo::MyForm form;
    Application::Run(%form);

}
